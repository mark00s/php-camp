<?php 
class DB
{
	public $mysql;
	public $query;
	public $handle;
	public function __construct($host, $login, $password, $dbName){	
		$this->mysql = mysqli_connect($host,$login,$password, $dbName);
	}

	public function query($query){	
		if (mysqli_query($this->mysql,$query))
		{
			$this->query = mysqli_query($this->mysql,$query);
			return true;
		}	
		else
		{
			return false;
		}
		}

	public function getAffectedRows(){
		return mysqli_affected_rows($this->mysql);
	}

    public function getRow(){
		return mysqli_fetch_row($this->query);
	}

    public function getAllRows(){
		$results = array();
        while (($row = $this->getRow())) {
            $results[] = $row;
        }
        return $results;
	}
}


?>