<?php

require_once('ProductBase.php');

class Product extends ProductBase
{
    //private $weight;
/*
    public function __get($name)
    {
        if (property_exists($this, $name)) {
            return $this->$name;
        }
    }

    public function __set($name, $value)
    {
        if (property_exists($this, $name)) 
        {
            $this->$name = $value;
        }   
    }

    public function getWeight()
    {
        return $this->weight;
    }
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }
    */
    public function __call($name, $arguments)
    {
        if ($name[0].$name[1].$name[2] == "set")
        {
            $name = substr($name, 3);
            $name = strtolower($name);
            $this->$name = $arguments[0];
        }
        else if ($name[0].$name[1].$name[2] == "get")
        {
            $name = substr($name, 3);
            $name = strtolower($name);
            return $this->$name;
        }
    }
}
?>